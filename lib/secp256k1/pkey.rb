require 'securerandom'

module Secp256k1
  class PKey
    def initialize(uncompressed)
      @raw = uncompressed.force_encoding('BINARY')
    end

    def to_s
      @to_s ||= to_compressed
    end

    def uncompressed
      @raw
    end

    def +(other)
      cptrs = [to_pointer, other.send(:to_pointer)]
      cptrs_ptr = FFI::MemoryPointer.new(:pointer, 2)

      cptrs_ptr.put_pointer(0, cptrs[0])
      cptrs_ptr.put_pointer(8, cptrs[1])

      out = FFI::MemoryPointer.new(64)

      valid = Secp256k1.secp256k1_ec_pubkey_combine(
        Secp256k1.context,
        out,
        cptrs_ptr,
        2
      )

      result = PKey.new(out.read_bytes(64)) if valid == 1

      out.free
      cptrs[0].free
      cptrs[1].free
      cptrs_ptr.free

      result
    end

    def ==(other)
      @raw == other.uncompressed
    end

    private

    # Caller must call #free
    def to_pointer
      out = FFI::MemoryPointer.new(64)
      out.write_bytes(@raw)
      out
    end

    def to_compressed
      cptr = to_pointer

      out = FFI::MemoryPointer.new(:char, 33)
      size_pointer = FFI::MemoryPointer.new(:long)
      size_pointer.write(:long, 33)
      Secp256k1.secp256k1_ec_pubkey_serialize(
        Secp256k1.context, out, size_pointer, cptr, 0x102
      )

      result = out.read_bytes(33)

      out.free
      size_pointer.free
      cptr.free

      result
    end
  end
end
