require 'securerandom'

module Secp256k1
  class Key
    def initialize(key = nil)
      if key
        if valid_key?(key)
          @key = key.force_encoding('BINARY')
        else
          raise ArgumentError, "Invalid secp256k1 private key"
        end
      else
        @key = random_key
      end
    end

    def private_key
      @key
    end

    def public_key
      PKey.new(to_pubkey)
    end

    def ==(other)
      @key == other.private_key
    end

    private

    def random_key
      loop do
        key = SecureRandom.random_bytes(32)
        return key if valid_key?(key)
      end
    end

    def valid_key?(key)
      kptr = Secp256k1.key_to_pointer(key)
      valid = Secp256k1.secp256k1_ec_seckey_verify(Secp256k1.context, kptr) == 1

      kptr.free

      valid
    end

    # Caller must call #free
    def to_pubkey
      out = FFI::MemoryPointer.new(64)
      priv = Secp256k1.key_to_pointer(@key)
      Secp256k1.secp256k1_ec_pubkey_create(Secp256k1.context, out, priv)

      val = out.read_bytes(64)
      out.free
      priv.free

      val
    end
  end
end
