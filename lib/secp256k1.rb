require 'ffi'

require_relative './secp256k1/key'
require_relative './secp256k1/pkey'
module Secp256k1
  extend FFI::Library
  ffi_lib 'secp256k1'

  ORDER = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141

  # Args:
  #   int: flags for context initialization. 0x301 prepares the context for signing and verifying
  attach_function :secp256k1_context_create, [:int], :pointer

  # Args:
  #   pointer (1): context
  #   pointer (2): 32 byte buffer with the private key
  # Returns
  #   1 if the private key is valid
  attach_function :secp256k1_ec_seckey_verify, %i[pointer pointer], :int

  # Args:
  #   pointer (1): context
  #   pointer (2): out pointer
  #   pointer (3): private key
  # Returns
  #   1 if the private key is valid
  attach_function :secp256k1_ec_pubkey_create, %i[pointer pointer pointer], :int

  # Args:
  #   pointer (1): context
  #   pointer (2): out buffer
  #   pointer (3): out buffer size
  #   pointer (4): uncompressed public key
  #   int: flags. 0x102 means "compress"
  attach_function :secp256k1_ec_pubkey_serialize, %i[pointer pointer pointer pointer int], :int

  # Args:
  #   pointer (1): context
  #   pointer (2): resulting public key
  #   pointer (3): pointer to array of pointers to public keys
  #   long (4): number of public keys
  # Returns
  #   1 on success
  attach_function :secp256k1_ec_pubkey_combine, %i[pointer pointer pointer long], :int

  def self.context
    @context ||= secp256k1_context_create(0x301)
  end

  def self.key_to_pointer(key)
    ptr = FFI::MemoryPointer.new(:uint8, 32)
    ptr.write_bytes(key)
    ptr
  end
end
