lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "secp256k1/version"

Gem::Specification.new do |spec|
  spec.name          = "curve-secp256k1"
  spec.version       = Secp256k1::VERSION
  spec.authors       = ["Crypto Jane"]
  spec.email         = ["cryptojanedoe@protonmail.com"]

  spec.summary       = 'Ruby wrapper for secp256k1 elliptic curve library. Commonly used to manage Cryptocurrency addresses'
  spec.homepage      = "https://gitlab.com/cryptojane/secp256k1"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = Dir["LICENSE.md", "README.md", "lib/**/*"]
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency     "ffi"
  spec.add_development_dependency "rspec"
end
