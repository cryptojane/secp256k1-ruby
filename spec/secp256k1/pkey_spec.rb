require 'spec_helper'

RSpec.describe Secp256k1::Key do
  let(:private_key) do
    Secp256k1::Key.new(
      "\x00\x00\x00\x00\x00\x00\x00\x00" \
      "\x00\x00\x00\x00\x00\x00\x00\x00" \
      "\x00\x00\x00\x00\x00\x00\x00\x00" \
      "\x00\x00\x00\x00\x00\x00\x00\x01"
    )
  end

  let(:public_key) { private_key.public_key }

  let(:raw_public_key) do
    "\x98\x17\xF8\x16[\x81\xF2Y" \
    "\xD9(\xCE-\xDB\xFC\x9B\x02" \
    "\a\v\x87\xCE\x95b\xA0U" \
    "\xAC\xBB\xDC\xF9~f\xBEy" \
    "\xB8\xD4\x10\xFB\x8F\xD0G\x9C" \
    "\x19T\x85\xA6H\xB4\x17\xFD" \
    "\xA8\b\x11\x0E\xFC\xFB\xA4]" \
    "e\xC4\xA3&w\xDA:H".force_encoding('BINARY')
  end

  let(:compressed_public_key) do
    "\x02y" \
    "\xbef~\xf9\xdc\xbb\xacU" \
    "\xa0b\x95\xce\x87\x0b\x07\x02" \
    "\x9b\xfc\xdb-\xce(\xd9Y" \
    "\xf2\x81[\x16\xf8\x17\x98".force_encoding('BINARY')
  end

  describe '#to_s' do
    it 'returns the compressed serialized form of the public key' do
      expect(public_key.to_s).to eq compressed_public_key
    end
  end

  describe '#uncompressed' do
    it 'returns the raw serialization of the point' do
      expect(public_key.uncompressed).to eq raw_public_key
    end
  end

  describe '#+' do
    context 'when summing the public keys of two different keys' do
      let(:k1) { Secp256k1::Key.new("\x00" * 31 + "\x01") }
      let(:k2) { Secp256k1::Key.new("\x00" * 31 + "\x02") }
      let(:k3) { Secp256k1::Key.new("\x00" * 31 + "\x03") }

      let(:pk1) { k1.public_key }
      let(:pk2) { k2.public_key }

      it 'returns the public key of a private key which is the sum of the first two' do
        expect(pk1 + pk2).to eq k3.public_key
      end
    end
  end
end
