require 'spec_helper'

RSpec.describe Secp256k1::Key do
  describe '#initialize' do
    context 'when called with no argument' do
      it 'generates a new random private key' do
        key = Secp256k1::Key.new
        expect(key.private_key).to_not be_nil
      end
    end

    context 'when called with a valid 256-bit private key argument' do
      let(:valid_key) do
        "\x00\x00\x00\x00\x00\x00\x00\x00" \
        "\x00\x00\x00\x00\x00\x00\x00\x00" \
        "\x00\x00\x00\x00\x00\x00\x00\x00" \
        "\x00\x00\x00\x00\x00\x00\x00\x01"
      end

      it 'is successful' do
        expect { Secp256k1::Key.new(valid_key) }.to_not raise_error
      end
    end

    context 'when called with an invalid private key argument' do
      let(:invalid_key) do
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF" \
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF" \
        "\xFF\xFF\xFF\xFF\xFF\xFF\xFF\xFF" \
        "\x00\x00\x00\x00\x00\x00\x00\x00"
      end

      it 'raises an error' do
        expect { Secp256k1::Key.new(invalid_key) }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#public_key' do
    it 'returns a PKey object' do
      key = Secp256k1::Key.new
      public_key = key.public_key

      expect(public_key).to be_a Secp256k1::PKey
    end
  end
end
