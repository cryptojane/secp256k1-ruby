# secp256k1 wrapper for Ruby

This gem wraps libsecp256k1 from the Bitcoin source in a couple of Ruby classes.

Use in a pipeline to generate crypto-currency addresses and for BIP32 operations (public key addition).

**To use this gem, libsecp256k1 must be installed on your system**

## Usage

Generate a new private key:

    key = Secp256k1::Key.new

Get a compressed public key (used by most cryptocurrencies):

    key.public_key

Load an existing private key:

    key = Secp256k1::Key.new("256-bit binary private key value")

## Installing secp256k1

### On a Debian/Ubuntu:

    sudo apt install libsecp256k1-dev

### On macOs:

It still doesn't come packaged with brew by default but there are a few taps where you can find it.

### Install from source:

    git clone https://github.com/bitcoin-core/secp256k1
    cd secp256k1
    ./autogen.sh
    ./configure --enable-module-recovery --enable-experimental --enable-module-ecdh --enable-module-schnorr
    make && sudo make install

## TODO

- Signing and verification
- Embed libsecp256k1 in `ext`? (as a git submodule)
